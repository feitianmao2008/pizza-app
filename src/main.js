import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import { routes } from './routes'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: routes,
  mode: 'history',
})

router.beforeEach((to, from, next)=>{
  if (to.path == '/login' || to.path == '/register') {
    next()
  } else {
    // next('/login');
    next()
  }
})

new Vue({
  router,
  el: '#app',
  render: h => h(App),

})
